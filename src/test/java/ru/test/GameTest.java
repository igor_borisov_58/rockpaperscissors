package ru.test;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.test.players.PaperShowingPlayer;
import ru.test.players.RandomPlayer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    public void shouldRunGame() {
        Player p1 = new PaperShowingPlayer();
        Player p2 = new RandomPlayer();
        Rules rules = new Rules();
        Game game = new Game(10, p1, p2, rules);
        GameResult result = game.run();
        assertNotNull(result);
    }

    @Test
    public void shoudRunGameWithMock() {
        Player p1 = Mockito.mock(Player.class);
        when(p1.getResult()).thenReturn(Result.SCISSORS);

        Player p2 = new PaperShowingPlayer();

        Rules rules = new Rules();
        Game game = new Game(10, p1, p2, rules);
        GameResult result = game.run();

        assertNotNull(result);
        assertEquals(10, result.getPlayer1WinCount());
        assertEquals(0, result.getPlayer2WinCount());
        assertEquals(0, result.getTieCount());
        assertEquals(10, result.getTotalCount());
    }
}
